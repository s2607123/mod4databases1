import java.sql.*;

public class exercise5 {
    public static void main(String[] args) {
        String username = "dab_di21222b_182";
        String password = System.getenv("DBpassword");

        try {
            Class c = Class.forName("org.postgresql.Driver");
            String host = "bronto.ewi.utwente.nl";
            String dbName = "dab_di21222b_182";
            int port = 5432;
            String currentSchema = "movies";
            String url = "jdbc:postgresql://" + host + ":" + port + "/" + dbName + "?currentSchema=" + currentSchema;
            Connection connection = DriverManager.getConnection(url, username, password);
            Statement statement = connection.createStatement();
            String query =  "SELECT DISTINCT p.name \n" +
                            "FROM person AS p, person AS p1, movie AS m, writes AS w, acts AS a \n" +
                            "WHERE p.pid = w.pid " +
                                "AND w.mid = m.mid " +
                                "AND p1.name = 'Harrison Ford' " +
                                "AND a.pid = p1.pid " +
                                "AND a.mid = m.mid;";
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                System.out.println(resultSet.getString(1));
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

}
