import java.sql.*;

public class exercise6 {
    public static void main(String[] args) {
        String username = "dab_di21222b_182";
        String password = System.getenv("DBpassword");

        try {
            Class c = Class.forName("org.postgresql.Driver");
            String host = "bronto.ewi.utwente.nl";
            String dbName = "dab_di21222b_182";
            int port = 5432;
            String currentSchema = "movies";
            String url = "jdbc:postgresql://" + host + ":" + port + "/" + dbName + "?currentSchema=" + currentSchema;
            Connection connection = DriverManager.getConnection(url, username, password);
            String query =  "SELECT DISTINCT p.name \n" +
                            "FROM person AS p, person AS p1, movie AS m, writes AS w, acts AS a \n" +
                            "WHERE p.pid = w.pid " +
                                "AND w.mid = m.mid " +
                                "AND p1.name = ? " +
                                "AND a.pid = p1.pid " +
                                "AND a.mid = m.mid;";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, "Bruce Willis");
            ResultSet resultSet = statement.executeQuery();
            int i = 1;
            while (resultSet.next()) {
                System.out.println(i + ". " + resultSet.getString(1));
                i++;
            }
            connection.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

}
