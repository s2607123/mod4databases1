import org.jetbrains.annotations.NotNull;

import java.sql.*;

public class exercise11 {
    public static void main(String @NotNull [] args) {
        String username = "dab_di21222b_182";
        String password = System.getenv("DBpassword");

        try {
            Class c = Class.forName("org.postgresql.Driver");
            String host = "bronto.ewi.utwente.nl";
            String dbName = "dab_di21222b_182";
            int port = 5432;
            String currentSchema = "movies";
            String url = "jdbc:postgresql://" + host + ":" + port + "/" + dbName + "?currentSchema=" + currentSchema;
            Connection connection = DriverManager.getConnection(url, username, password);

            String query =  "SELECT authorsonlymoviesnodirector()";
            int iters=100;
            long startTime = System.currentTimeMillis();
            PreparedStatement statement = connection.prepareStatement(query);
            for (int i=0; i<iters; i++) {
                statement.executeQuery();
            }
            long stopTime = System.currentTimeMillis();
            double elapsedTime = (stopTime - startTime) / (1.0*iters);
            System.out.println("Measured time: "+elapsedTime+" ms");
            connection.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
}
