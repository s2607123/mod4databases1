import org.jetbrains.annotations.NotNull;

import java.sql.*;

public class exercise8 {
    public static void main(String @NotNull [] args) {
        String username = "dab_di21222b_182";
        String password = System.getenv("DBpassword");

        String name = args[0] + " " + args[1];

        try {
            Class c = Class.forName("org.postgresql.Driver");
            String host = "bronto.ewi.utwente.nl";
            String dbName = "dab_di21222b_182";
            int port = 5432;
            String currentSchema = "movies";
            String url = "jdbc:postgresql://" + host + ":" + port + "/" + dbName + "?currentSchema=" + currentSchema;
            Connection connection = DriverManager.getConnection(url, username, password);
            String query =  "SELECT moviesofactor('" + name + "');";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            int i = 1;
            while (resultSet.next()) {
                System.out.println(i + ". " + resultSet.getString(1));
                i++;
            }
            connection.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

}
